import hashlib
import base64
import string
from collections import defaultdict
from itertools import chain, permutations, combinations, imap
from gautils import event_occured, weighted_choice, \
        random_string, string_mutation, split_list_crossover, \
        string_dist, EnumGenome

class HTTPMethod(EnumGenome):
    MAX_LEN = 20

    VALUES = {
        'GET'     : 10,
        'POST'    : 10,
        'PUT'     : 10,
        'DELETE'  : 10,
        'HEAD'    : 10,
        'OPTIONS' : 10,
        'random'  : 0.1,
    }

    def __init__(self, value=None, random=None):
        EnumGenome.__init__(self, value, random)

class URI:
    ALLOWED = string.letters + string.digits + '-%&.=/'
    MALFORMED_PROB = 0.05
    FLIP_PROB = 0.05
    MAX_LEN = 500

    def __init__(self, uri=None, random=None):
        if uri is None:
            if event_occured(random, URI.MALFORMED_PROB):
                self.uri = random_string(random, URI.MAX_LEN)
            else:
                self.uri = '/' + random_string(random, URI.MAX_LEN - 1, allowed=URI.ALLOWED)
        else:
            self.uri = uri[:URI.MAX_LEN]

    def mutate(self, random):
        if event_occured(random, URI.MALFORMED_PROB):
            return URI(string_mutation(random, self.uri, flip_prob=URI.FLIP_PROB))
        else:
            return URI('/' + string_mutation(random, self.uri, flip_prob=URI.FLIP_PROB, allowed=URI.ALLOWED)[1:])

    @staticmethod
    def crossover(random, mom, dad):
        res1, res2 = split_list_crossover(random, mom.uri, dad.uri, max_len=URI.MAX_LEN)
        return (URI(uri=res1), URI(uri=res2))

    @staticmethod
    def dist(first, second):
        return string_dist(first.uri, second.uri)
    
    def __str__(self):
        return self.uri


class HTTP(EnumGenome):
    MAX_LEN = 10
    VALUES = {
        'HTTP' : 10,
        'random' : 0.1
    }

    def __init__(self, value=None, random=None):
        EnumGenome.__init__(self, value, random)

class HTTPVersion(EnumGenome):
    MAX_LEN = 7
    VALUES = {
        '0.9' : 1,
        '1.0' : 10,
        '1.1' : 10,
        'random' : 0.1
    }

    def __init__(self, value=None, random=None):
        EnumGenome.__init__(self, value, random)

class HTTPStartingLine:
    METHOD_WEIGHT = 1.0
    URI_WEIGHT = 1.0
    HTTP_WEIGHT = 0.2
    VER_WEIGHT = 0.2

    def __init__(self, method=None, uri=None, http=None, ver=None, random=None):
        if method is None:
            self.method = HTTPMethod(random=random) 
        else:
            self.method = method
        if uri is None:
            self.uri = URI(random=random) 
        else:
            self.uri = uri
        if http is None:
            self.http = HTTP(random=random) 
        else:
            self.http = http
        if ver is None:
            self.ver = HTTPVersion(random=random) 
        else:
            self.ver = ver
        self.result = str(self.method) + ' ' + str(self.uri) + ' ' + str(self.http) + '/' + str(self.ver)

    def mutate(self, random):
        method = self.method.mutate(random)
        uri = self.uri.mutate(random)
        http = self.http.mutate(random)
        ver = self.ver.mutate(random)
        return HTTPStartingLine(method, uri, http, ver)

    @staticmethod
    def crossover(random, mom, dad):
        method1, method2 = HTTPMethod.crossover(random, mom.method, dad.method)
        uri1, uri2 = URI.crossover(random, mom.uri, dad.uri)
        http1, http2 = HTTP.crossover(random, mom.http, dad.http)
        ver1, ver2 = HTTPVersion.crossover(random, mom.ver, dad.ver)
        return (HTTPStartingLine(method1, uri1, http1, ver1), HTTPStartingLine(method2, uri2, http2, ver2))

    @staticmethod
    def dist(first, second):
        return HTTPStartingLine.METHOD_WEIGHT * HTTPMethod.dist(first.method, second.method) + \
                HTTPStartingLine.URI_WEIGHT * URI.dist(first.uri, second.uri) + \
                HTTPStartingLine.HTTP_WEIGHT * HTTP.dist(first.http, second.http) + \
                HTTPStartingLine.VER_WEIGHT * HTTPVersion.dist(first.ver, second.ver)

    def __str__(self):
        return self.result

class HTTPHeaderName(EnumGenome):
    MAX_LEN = 20
    VALUES = {
        'Accept' : 1,
        'Accept-Charset' : 1,
        'Accept-Encoding' : 1,
        'Accept-Language' : 1,
        'Authorization' : 1,
        'Cache-Control' : 1,
        'Connection' : 1,
        'Content-Disposition' : 1,
        'Content-Encoding' : 1,
        'Content-Language' : 1,
        'Content-Location' : 1,
        'Content-Range' : 1,
        'Content-Type' : 1,
        'Content-Version' : 1,
        'Date' : 1,
        'Derived-From' : 1,
        'Expect' : 1,
        'Expires' : 1,
        # 'From' : 1,
        'Host' : 1,
        'If-Match' : 1,
        'If-Modified-Since' : 1,
        'If-None-Match' : 1,
        'If-Range' : 1,
        'If-Unmodified-Since' : 1,
        'Link' : 1,
        'Max-Forwards' : 1,
        'MIME-Version' : 1,
        'Pragma' : 1,
        'Proxy-Authorization' : 1,
        'Range' : 1,
        'Referer' : 1,
        'Title' : 1,
        'TE' : 1,
        'Trailer' : 1,
        'Transfer-Encoding' : 1,
        'Upgrade' : 1,
        'User-Agent' : 1,
        'Via' : 1,
        'Warning' : 1,
        'random' : 0.1,
    }

    def __init__(self, value=None, random=None):
        EnumGenome.__init__(self, value, random)

class HTTPHeader:
    MAX_VALUE_LEN = 30
    ALLOWED = string.digits + string.letters + string.punctuation

    def __init__(self, name=None, value=None, random=None):
        if name is None:
            self.name = HTTPHeaderName(random=random)
        else:
            self.name = name
        if value is None:
            self.value = random_string(random, HTTPHeader.MAX_VALUE_LEN, allowed=HTTPHeader.ALLOWED)
        else:
            self.value = value
        self.result = str(self.name) + ': ' + str(self.value)

    def mutate(self, random):
        name = self.name.mutate(random)
        value = string_mutation(random, self.value, allowed=HTTPHeader.ALLOWED)
        return HTTPHeader(name, value)

    @staticmethod
    def crossover(random, mom, dad):
        name1, name2 = HTTPHeaderName.crossover(random, mom.name, dad.name)
        value1, value2 = split_list_crossover(random, mom.value, dad.value, max_len=HTTPHeader.MAX_VALUE_LEN)
        return (HTTPHeader(name1, value1), HTTPHeader(name2, value2))

    def __str__(self):
        return self.result

class HTTPHeaderList:
    CONTENT_LENGTH_OPTIONS = {
        'true' : 10,
        'false' : 0.1,
        'empty' : 0.1,
    }
    MD5_OPTIONS = {
        'true' : 10,
        'false' : 0.1,
        'empty' : 20,
    }
    MAX_HEADER_LIST_LEN = 10
    HEADER_WEIGHT = 1

    def __init__(self, random, body, headers=None, header_list=None):
        if header_list:
            self.headers = header_list
        else:
            hd_len = 0
            if headers:
                self.headers = headers
                hd_len = len(headers)
            else:
                self.headers = list()
            headers_len = random.randint(hd_len, HTTPHeaderList.MAX_HEADER_LIST_LEN)
            self.headers.extend([HTTPHeader(random=random) for i in range(headers_len)])
        self.special_headers = list()
        cl_choice = weighted_choice(random, HTTPHeaderList.CONTENT_LENGTH_OPTIONS)
        if cl_choice == 'true':
            self.special_headers.append(HTTPHeader(name='Content-Length', value=str(len(body))))
        elif cl_choice == 'false':
            self.special_headers.append(HTTPHeader(name='Content-Length', random=random))
        ch_choice = weighted_choice(random, HTTPHeaderList.MD5_OPTIONS)
        if ch_choice == 'true':
            self.special_headers.append(HTTPHeader(name='Content-MD5', value=base64.b64encode(hashlib.md5(body).digest())))
        elif ch_choice == 'false':
            self.special_headers.append(HTTPHeader(name='Content-MD5', random=random))
        self.result = '\r\n'.join(map(str, self.headers + self.special_headers))

    def mutate(self, random, body):
        headers = [header.mutate(random) for header in self.headers]
        return HTTPHeaderList(random, body, header_list=headers)

    @staticmethod
    def crossover(random, mom, dad, body1, body2):
        headers1, headers2 = split_list_crossover(random, mom.headers, dad.headers, HTTPHeaderList.MAX_HEADER_LIST_LEN)
        return (HTTPHeaderList(random, body1, headers=headers1), HTTPHeaderList(random, body2, headers=headers2))

    @staticmethod
    def dist(first, second):
        fst = defaultdict(list)
        snd = defaultdict(list)
        for header in first.headers:
            fst[header.name].append(header.value)
        for header in second.headers:
            snd[header.name].append(header.value)
        names = set(fst.keys() + snd.keys())
        diff = 0.0
        for name in names:
            if len(fst[name]) < len(snd[name]):
                lt = fst[name]
                gt = snd[name]
            else:
                lt = snd[name]
                gt = fst[name]
            n = len(lt)
            comb = chain(*map(permutations, combinations(gt, n)))
            diff += HTTPHeaderList.HEADER_WEIGHT * (len(gt) - len(lt))
            diff += min(map(lambda x: sum(imap(string_dist, x, lt)), comb))
        diff /= max(len(first.headers), len(second.headers))
        return diff


    def __str__(self):
        return self.result

class HTTPMessage:
    MAX_BODY_LEN = 1000
    CURRENT_ID = 0
    STARTING_LINE_WEIGHT = 1.0
    HEADER_LIST_WEIGHT = 1.0
    BODY_WEIGHT = 0.2

    def __init__(self, starting_line=None, headers=None, header_list=None, body=None, random=None):
        if starting_line is None:
            self.starting_line = HTTPStartingLine(random=random)
        else:
            self.starting_line = starting_line
        if body is None:
            body_len = random.randint(0, HTTPMessage.MAX_BODY_LEN)
            self.body = random_string(random, body_len)
        else:
            self.body = body
        if header_list:
            self.header_list = header_list
        else:
            self.header_list = HTTPHeaderList(random, self.body, headers=headers)
        self.result = str(self.starting_line) + '\r\n' + str(self.header_list) + '\r\n\r\n' + str(self.body)
        self._id = HTTPMessage.CURRENT_ID
        HTTPMessage.CURRENT_ID += 1

    def mutate(self, random):
        starting_line = self.starting_line.mutate(random)
        body = string_mutation(random, self.body)
        header_list = self.header_list.mutate(random, body)
        return HTTPMessage(starting_line=starting_line, header_list=header_list, body=body)

    def without_body(self):
        return str(self.starting_line) + '\n' + str(self.header_list) + '\n\n'

    @staticmethod
    def crossover(random, mom, dad):
        starting_line1, starting_line2 = HTTPStartingLine.crossover(random, mom.starting_line, dad.starting_line)
        body1, body2 = split_list_crossover(random, mom.body, dad.body)
        header_list1, header_list2 = HTTPHeaderList.crossover(random, mom.header_list, dad.header_list, body1, body2)
        return (HTTPMessage(starting_line=starting_line1, header_list=header_list1, body=body1), HTTPMessage(starting_line=starting_line2, header_list=header_list2, body=body2))

    @staticmethod
    def dist(first, second):
        return HTTPMessage.STARTING_LINE_WEIGHT * HTTPStartingLine.dist(first.starting_line, second.starting_line) + \
                HTTPMessage.HEADER_LIST_WEIGHT * HTTPHeaderList.dist(first.header_list, second.header_list) + \
                HTTPMessage.BODY_WEIGHT * string_dist(first.body, second.body)

    def __str__(self):
        return self.result
