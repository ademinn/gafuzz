from gautils import Event

class Fuzzable:

    def __init__(self):
        pass


class List(Fuzzable, list):

    def __init__(self, delim=' ', *args):
        list.__init__(self, args)
        self.__delim = delim

    def __str__(self):
        return self.__delim.join(map(str, self))

    def set_delim(self, delim):
        self.__delim = delim

    def mutate(self):
        for item in self:
            item.mutate()

class WeakList(Fuzzable, list):

    def __init__(self, delim=' ', *args):
        list.__init__(self, args)
        self.__delim = delim

    def append(self, value, prob=0.5):
        super(WeakList, self).append( (value, prob) )

    def mutate(self):
        pass

    @staticmethod
    def merge(first, second, first_prob=0.5):
        pass
