import logging
import random
import copy_reg
import multiprocessing.reduction as mpr
import multiprocessing as mp
from multiprocessing import Queue, Process, Pool
from multiprocessing.managers import SyncManager
from optparse import OptionParser
from inspyred.ec import EvolutionaryComputation
from inspyred.ec.evaluators import parallel_evaluation_mp, evaluator
from inspyred.ec.variators import crossover, mutator
from inspyred.ec.selectors import tournament_selection, default_selection
from inspyred.ec.replacers import generational_replacement, random_replacement, crowding_replacement
from inspyred.ec.terminators import user_termination

from http import HTTPMessage, URI, HTTPStartingLine, HTTPHeaderName, HTTPHeader
from environment import Environment, Task

line_coverage = list()
current_coverage = list()
covered = 0
gen_count = 0

def replacer(random, population, parents, offspring, args):
    answer = crowding_replacement(random, population, parents, offspring, args)
    global line_coverage
    line_coverage = list(current_coverage)
    return answer

def simple_replacer(random, population, parents, offspring, args):
    answer = generational_replacement(random, population, parents, offspring, args)
    global line_coverage
    line_coverage = list(current_coverage)
    return answer

@crossover
def http_cross(random, mom, dad, args):
    return HTTPMessage.crossover(random, mom, dad)

@mutator
def http_mutate(random, candidate, args):
    return candidate.mutate(random)

def calc_cov(candidate, queue):
    in_pipe, out_pipe = mp.Pipe()
    queue.put((candidate, mpr.reduce_connection(out_pipe)), block=True)
    return in_pipe.recv()

def fitness(new, result, lines_count):
    answer = 1000000
    global current_coverage
    global covered
    for i in range(lines_count):
        if result[i] != 0:
            if new:
                if current_coverage[i] == 0:
                    covered += 1
                current_coverage[i] += 1
            if answer > line_coverage[i]:
                answer = line_coverage[i]
    return answer

def evaluation(candidates, args):
    queue = args['queue']
    pool = Pool(processes=args['env_count'])
    res = [pool.apply_async(calc_cov, (c, queue)) for c in candidates]
    pool.close()
    pool.join()
    #cov = pool.map(calc_cov, [(candidate, queue) for candidate in candidates], chunksize=args['size'])
    cov = [r.get() for r in res]
    lines_count = args['environ'].lines_count
    fit = list()
    for new, result in cov:
        fit.append(fitness(new, result, lines_count))
    return fit
    
#def evaluator(candidates, args):
#    tasks = list()
#    answer = list()
#    for candidate in candidates:
#        cond = args['manager'].Condition()
#        task = Task(candidate, cond)
#        args['queue'].put(candidate, True)
#        #args['queue'].put(task, True)
#        tasks.append(task)
#    for task in tasks:
#        task.cond.acquire()
#        while not task.ready:
#            print 'wait'
#            task.cond.wait()
#        new, result = task.result
#        task.cond.release()
#        answer.append(fitness(new, result, args))
#    return answer
        

def evaluate(candidate, args):
    return evaluation([candidate], args)[0]


def generator(random, args):
    msg = None
    global gen_count
    if gen_count < args['good']:
        uri = None
        if 'urls' in args:
            uri = URI(uri=args['urls'].readline()[:-1])
        msg = HTTPMessage(HTTPStartingLine(uri=uri, random=random), random=random)
    else:
        msg = HTTPMessage(random=random)
    gen_count += 1
    #print str(msg)
    return msg 

def termination(population, num_generations, num_evaluations, args):
    environ = args['environ']
    candidates = args['candidates']
    if environ.test_count() >= candidates:
        return True
    else:
        return False

def selection(random, population, args):
    fits = evaluation([elem.candidate for elem in population], args)
    for elem, fit in zip(population, fits):
        elem.fitness = fit
    return default_selection(random, population, args)

def observer(population, num_generations, num_evaluations, args):
    test_count = args['environ'].test_count()
    lines = args['environ'].lines_count
    best_fit = min([x for x, y in zip(line_coverage, current_coverage) if y != 0])
    #best_fit = filter(lambda (x, y): y != 0, zip(line_coverage, current_coverage))
    #best_fit = 10000
    #for elem in population:
    #    if elem.fitness < best_fit:
    #        best_fit = elem.fitness
    #logging.info(line_coverage)
    logging.info(current_coverage)
    s = 'generation: %6d | candidates: %7d | covered: %5d | lines: %5d | fitness: %5d' % (num_generations, test_count, covered, lines, best_fit)
    logging.info(s)
    if args.has_key('log'):
        args['log'].write(s + '\n')

def httpdist(first, second):
    return HTTPMessage.dist(first, second)

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-g', '--generations',
            type='int', default=1000, dest='generations',
            help='number of generations [default: %default]')
    parser.add_option('-c', '--candidates',
            type='int', default=100000, dest='candidates',
            help='total number of candidates [default: %default]')
    parser.add_option('--size',
            type='int', default=100, dest='size',
            help='generation size [default: %default]')
    parser.add_option('-e', '--environment',
            type='string', dest='environment',
            help='path to environment config file')
    parser.add_option('-t', '--tournament',
            type='int', default=3, dest='tournament',
            help='tournament size [default: %default]')
    parser.add_option('--simple',
            action='store_true', default=False, dest='simple',
            help='run without genetic algorithm')
    parser.add_option('--log',
            type='string', default=None, dest='log',
            help='log file')
    parser.add_option('--prefix',
            type='string', default='', dest='prefix',
            help='database collections prefix')
    parser.add_option('-m', '--mongo',
            type='int', default=27017, dest='mongo_port',
            help='mongo port [default: %default]')
    parser.add_option('-u', '--urls',
            type='string', default=None, dest='urls',
            help='path to file with urls')
    parser.add_option('-d', '--dist',
            type='int', default=10, dest='distance',
            help='crowding distance [default: %default]')
    parser.add_option('--good',
            type='int', default=None, dest='good',
            help='number of well formed requests [default: size / 2]')
    opts, args = parser.parse_args()
    logging.basicConfig(level=logging.INFO)
    if opts.good is None:
        oprts.good = opts.size / 4
    prcs = list()
    manager = SyncManager()
    manager.start()
    queue = manager.Queue()
    #events = manager.list([manager.Event() for i in range(opts.size)])
    in_pipes = manager.list()
    out_pipes = manager.list()
    for i in range(opts.size):
        in_pipe, out_pipe = mp.Pipe()
        in_pipes.append(mpr.reduce_connection(in_pipe))
        out_pipes.append(mpr.reduce_connection(out_pipe))
    environ = None
    env_count = 0
    with open(opts.environment, 'r') as env:
        for line in env:
            if environ is None:
                environ = Environment(line[:-1], opts.mongo_port, queue, prefix=opts.prefix)
            env_count += 1
            pr = Environment(line[:-1], opts.mongo_port, queue, prefix=opts.prefix, lines_count=environ.lines_count, id_=env_count)
            prcs.append(pr)
    #pool = Pool(prcs)
    for pr in prcs:
        pr.start()
    ids = manager.Queue()
    for i in range(opts.size):
        ids.put(i)
    global current_coverage
    global line_coverage
    current_coverage = [0] * environ.lines_count
    line_coverage = [0] * environ.lines_count
    rnd = random.Random()
    ec = EvolutionaryComputation(rnd)
    args = {
            'environ' : environ,
            'num_selected' : opts.size / 2,
            'tournament_size' : opts.tournament,
            'candidates' : opts.candidates,
            'crowding_distance' : opts.distance,
            'distance_function' : HTTPMessage.dist,
            'queue' : queue,
            'size' : opts.size,
            'good' : opts.good,
            'env_count' : env_count,
            }
    #args = manager.dict()
    #args['environ'] = environ
    #args['num_selected'] = opts.size / 3
    #args['tournament_size'] = opts.tournament
    #args['candidates'] = opts.candidates
    #args['crowding_distance'] = opts.distance
    #args['distance_function'] = httpdist #HTTPMessage.dist
    #args['queue'] = queue
    #args['manager'] = manager
    #args['ids'] = ids
    if opts.simple:
        ec.selector = default_selection
        ec.variator = http_mutate
        ec.replacer = simple_replacer
    else:
        ec.selector = selection
        ec.variator = [http_cross, http_mutate]
        ec.replacer = replacer
        args['num_elites'] = opts.size / 4
    ec.terminator = [termination]
    ec.observer = observer
    if opts.urls:
        args['urls'] = open(opts.urls, 'r')
    if opts.log is not None:
        with open(opts.log, 'w') as output:
            args['log'] = output
            #ec.evolve(generator, evaluate, opts.size, maximize=False, **args)
            ec.evolve(generator=generator, evaluator=evaluation, mp_evaluator=fitness, mp_num_cpus=len(prcs), pop_size=opts.size, maximize=False, **args)
    else:
        ec.evolve(generator=generator, evaluator=evaluation, mp_evaluator=fitness, mp_num_cpus=len(prcs), pop_size=opts.size, maximize=False, **args)
        #ec.evolve(generator, evaluate, opts.size, maximize=False, **args)
    for pr in prcs:
        pr.terminate()
        pr.join()
    #pool.terminate()
    manager.shutdown()
