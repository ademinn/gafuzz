import random
import http

TEST_COUNT = 100

r = random.Random()
for i in range(TEST_COUNT):
    a = http.HTTPMessage(random=r)
    b = http.HTTPMessage(random=r)
    c = a.mutate(r)
    (d1, d2) = http.HTTPMessage.crossover(r, b, c)
    print '%d finished' % i
print d2._id
