from Levenshtein import distance

def event_occured(random, prob):
    return prob > random.random()

def weighted_choice(random, elems):
    total = sum(elems.values())
    choice = random.uniform(0, total)
    current = 0
    for key, value in elems.iteritems():
        if current + value > choice:
            return key
        current += value

all_bytes = [chr(i) for i in range(255)]

def random_string(random, length, allowed=all_bytes):
    return ''.join([random.choice(allowed) for i in range(length)])

def string_mutation(random, string, flip_prob=0.1, allowed=all_bytes):
    lst = list(string)
    alwd_len = len(allowed) - 1
    for i in range(len(string)):
        if event_occured(random, flip_prob):
            lst[i] = allowed[random.randint(0, alwd_len)]
    return ''.join(lst)

def split_list_crossover(random, mom, dad, max_len=None, mom_fst_prob=0.5):
    if event_occured(random, mom_fst_prob):
        fst = mom
        snd = dad
    else:
        fst = dad
        snd = mom
    fst_split = 0 if len(fst) == 0 else random.randint(0, len(fst))
    snd_split = 0 if len(snd) == 0 else random.randint(0, len(snd))
    res1 = fst[:fst_split] + snd[snd_split:]
    res2 = snd[:snd_split] + fst[fst_split:]
    if max_len is not None:
        res1 = res1[:max_len]
        res2 = res2[:max_len]
    return (res1, res2)

def string_dist(first, second):
    return distance(first, second) * 1.0 / max(len(first), len(second))

class EnumGenome:
    MOM_PROB = 0.5
    MUTATE_PROB = 0.1
    MAX_LEN = 20

    VALUES = {
        'random' : 1,
    }


    def __init__(self, value=None, random=None):
        if value is None:
            value = self.generate(random)
        self.value = value

    def mutate(self, random):
        if event_occured(random, self.MUTATE_PROB):
            return self.construct(random=random)
        else:
            return self

    @classmethod
    def construct(cls, value=None, random=None):
        return cls(value, random)

    @classmethod
    def generate(cls, random):
        value = weighted_choice(random, cls.VALUES)
        if value == 'random':
            value = cls.random_value(random)
        return value

    @classmethod
    def random_value(cls, random):
        return random_string(random, cls.MAX_LEN)

    @classmethod
    def crossover(cls, random, mom, dad):
        if event_occured(random, cls.MOM_PROB):
            return (mom, dad)
        else:
            return (dad, mom)

    @staticmethod
    def dist(first, second):
        return 0 if first.value == second.value else 1

    def __str__(self):
        return self.value
