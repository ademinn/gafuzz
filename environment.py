import logging
import signal
import httplib
import errno
import os
import re
import time
import shutil
import socket
import tempfile
import subprocess
from pymongo import MongoClient
from bson.binary import Binary
import multiprocessing as mp
#from multiprocessing import Process

class Task:
    def __init__(self, msg, cond):
        self.msg = msg
        self.cond = cond
        self.ready = False

    def set_result(self, result):
        self.result = result
        ready = True

class Environment(mp.Process):
    TIMEOUT = 60.0
    SOCK_TIMEOUT = 1.0
    BUF_SIZE = 100
    GCOV_RE = re.compile('^\s*([\d\-#]*):\s*(\d+)')
    LOG_LEVEL = logging.INFO

    def __init__(self, config, mongo_port, queue, prefix='', lines_count=None, id_=0):
        mp.Process.__init__(self)
        self.id_ = id_
        self.logger = logging.getLogger('environ %d' % self.id_)
        self.logger.setLevel(Environment.LOG_LEVEL)
        params = dict()
        execfile(config, params)
        self.devnull = open(os.devnull, 'w')
        self.cmd = params['cmd']
        self.host = socket.gethostbyname(socket.gethostname())
        self.port = params['port']
        self.src = params['src']
        self.gcov = params['gcov']
        client = MongoClient('localhost', mongo_port)
        db = client['gafuzz']
        self.results = db[prefix + 'results']
        self.results.remove()
        self.faults = db[prefix + 'faults']
        self.faults.remove()
        self.tempdir = tempfile.mkdtemp()
        self.queue = queue
        if lines_count is None:
            self.line_map = db['map']
            lines = self.get_lines()
            line_map = []
            for i, (line_n, cov) in enumerate(lines):
                line_map.append({'ind': i, 'line': line_n})
            self.line_map.insert({'line_map' : line_map})
            self.lines_count = len(lines)
        else:
            self.lines_count = lines_count
        #self.pipes = pipes

    def run(self):
        while True:
            task, (func, args) = self.queue.get(True)
            self.logger.info('new task')
            pipe = func(*args)
            pipe.send(self.test(task))

    def get_lines(self):
        process = self.start_target(tag='get_lines')
        time.sleep(0.5)
        self.stop(process)
        return self.get_coverage(True)

    def test_count(self):
        return self.results.count()

    def test(self, message):
        result = self.results.find_one({'_id' : message._id})
        if result is None:
            try:
                for prefix, obj, src in self.src:
                    if os.path.exists(obj):
                        os.remove(obj)
                self.make_shot(str(message))
                result = self.get_coverage()
            except:
                self.faults.insert({'_id' : message._id})
                result = [0] * self.lines_count
            self.results.insert({'_id' : message._id, 'coverage' : result, 'msg' : Binary(str(message))})
            result = (True, result)
        else:
            result = (False, result['coverage'])
        return result

    def make_shot(self, data):
        try:
            process = self.start_target(tag='make_shot')
            #process = subprocess.Popen(self.cmd, stdout=self.devnull, stderr=self.devnull)
            #process = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            time.sleep(0.2)
            sock = socket.create_connection((self.host, self.port))
            sock.settimeout(self.SOCK_TIMEOUT)
            sock.sendall(data)
            sock.recv(self.BUF_SIZE)
        finally:
            self.stop(process)

    def get_coverage(self, line_no=False):
        cwd = os.getcwd()
        answer = list()
        for prefix, obj, src in self.src:
            dir_ = os.path.join(self.tempdir, prefix)
            if not os.path.exists(dir_):
                os.makedirs(dir_)
            os.chdir(dir_)
            subprocess.call(['gcov-4.4', '-o', obj, src], stdout=self.devnull)
        for gcov in self.gcov:
            answer.extend(Environment.parse_gcov(os.path.join(self.tempdir, gcov), line_no))
        os.chdir(cwd)
        return answer

    @staticmethod
    def parse_gcov(gcov, line_no):
        answer = list()
        with open(gcov, 'r') as inp:
            for line in inp:
                res = Environment.GCOV_RE.match(line)
                status = res.group(1)
                covered = 0
                if status == '-':
                    continue
                elif status != '#####':
                    covered = int(status)
                if line_no:
                    line_n = int(res.group(2))
                    answer.append((line_n, covered))
                else:
                    answer.append(covered)
        return answer

    def start_target(self, tag='default'):
        process = subprocess.Popen(self.cmd, stdout=self.devnull, stderr=self.devnull)
        #process = subprocess.Popen(self.cmd)

        st = time.time()

        ping = True

        while ping:
            try:
                sock = socket.create_connection((self.host, self.port))
                sock.sendall('GET /admin HTTP/1.1\r\n\r\n')
                s = sock.recv(self.BUF_SIZE)
                self.logger.info('Process started in %.2f s' % (time.time() - st))
                break
            except socket.error as e:
                if e.errno == errno.ECONNREFUSED:
                    pass
                else:
                    self.logger.error('Process exception: %s' % str(e))
                    self.stop(process)
                    return None
            #except Exception, ex:
            #    print('Process exception: %s' % str(ex))
            #    return None

            if time.time() - st > self.TIMEOUT:
                self.stop(process)
                return None

            time.sleep(5)

        if not ping:
            time.sleep(0.5)

        return process

    @staticmethod
    def stop(process):
        pid = process.pid
        process.terminate()
        time.sleep(0.5)
        if process.poll() is None:
            os.kill(pid, signal.SIGKILL)
            time.sleep(0.5)
            self.logger.warning('process not terminated')
            raise Exception('process not terminated')
        #if process.poll() is None:
        #    process.kill()
        #pid = process.pid
        #cmd = ['ps', '-eo', 'pid,ppid'] 
        #output = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0] 
        #output = output.split('\n')[1:] # skip the header 
        #children = list()
        #for row in output: 
        #    if not row: 
        #        continue 
        #    child_pid, parent_pid = row.split() 
        #    if parent_pid == str(pid): 
        #        child_pid = int(child_pid) 
        #        children.append(child_pid)
        #process.terminate()
        #for child_pid in children:
        #    os.kill(child_pid, signal.SIGTERM)


    def __del__(self):
        if self.tempdir is not None and os.path.exists(self.tempdir):
            shutil.rmtree(self.tempdir)
        self.devnull.close()
